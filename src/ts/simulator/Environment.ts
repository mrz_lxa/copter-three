import {Vec3d} from "./utils/Vec3d";


export class Environment{

    public static GRAVITY : Vec3d = new Vec3d(0,0,-9.8);

    public static DIR_FRONT: Vec3d = new Vec3d(1,0,0);
    public static DIR_RIGHT: Vec3d = new Vec3d(0,1,0);
    public static DIR_UP: Vec3d = new Vec3d(0,0,1);


    public static AIR_DENCITY: number = 1.225;


    public static PIXELS_IN_METER = 100;

}