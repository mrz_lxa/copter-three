
export class Vec2d {

    public x: number;
    public y: number;

    constructor(x: number, y: number){
        this.x = x;
        this.y = y;
    }

    add(vec : Vec2d): Vec2d{
        this.x += vec.x;
        this.y += vec.y;

        return this;
    }


    set(x:number, y: number): Vec2d;
    set(vec: Vec2d): Vec2d;
    set(arg1:any, arg2?:number){

        //first implementation with 2 number args.
        if(typeof(arg1) === "number"){
            this.x = arg1;
            this.y = arg2;

            return this;
        }

        //second implementation with vec2d arg.
        this.x = arg1.x;
        this.y = arg1.y;

        return this;
    }

    mul(scalarValue: number): Vec2d{
        this.x *= scalarValue;
        this.y *= scalarValue;

        return this;
    }

    get length(){
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }

    toString(): string{
        return `{${this.x.toFixed(3)}, ${this.y.toFixed(3)}}`;
    }

}