import {Vec2d} from "./Vec2d";

export class Vec3d {

    public x: number;
    public y: number;
    public z: number;


    get xy(): Vec2d{
        return new Vec2d(this.x, this.y);
    }

    constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    add(vec: Vec3d): Vec3d {
        this.x += vec.x;
        this.y += vec.y;
        this.z += vec.z;

        return this;
    }


    set(x: number, y: number, z: number): Vec3d;
    set(x: Vec3d): Vec3d;
    set(arg1:any, arg2?:number, arg3?:number):Vec3d{

        if(arg1 instanceof Vec3d){
            this.x = arg1.x;
            this.y = arg1.y;
            this.z = arg1.z;

            return this;
        }

        this.x = arg1;
        this.y = arg2;
        this.z = arg3;

        return this;
    }

    mul(scalarValue: number): Vec3d {
        this.x *= scalarValue;
        this.y *= scalarValue;
        this.z *= scalarValue;

        return this;
    }


    get length(): number{
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }


    toString(): string {
        return `{${this.x.toFixed(3)}, ${this.y.toFixed(3)}, ${this.z.toFixed(3)}}`;
    }
}