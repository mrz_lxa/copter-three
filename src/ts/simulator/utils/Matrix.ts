
const MAT3D_ROWS = 3;
const MAT3D_COLS = 3;

export class Matrix extends Array<Array<number>>{

    constructor(rows, cols:number, zero?:boolean){
        super(rows);


        for(let i=0; i<rows; i++){
            let row = new Array(cols);

            for(let j=0; j<row.length; j++){
                row[j] = (j==i && !zero)? 1 : 0;
            }

            this[i] = (row);
        }

        (<any>Object).setPrototypeOf(this, Matrix.prototype);
    }

    static createMat3d(): Matrix{
        return new Matrix(MAT3D_ROWS, MAT3D_COLS);
    }

    public set(data: Array<Array<number>>){
        let dataRow: Array<number>;

        if(!data || data.length != this.getRows()){
            throw new Error("Data has invalid rows count or doesn't exist.");
        }

        for(let i=0; i<data.length; i++){
            dataRow = data[i];

            if(!dataRow || (dataRow.length != this.getCols())){
                throw new Error("Data row doesn't fit matrix (row length mismatch) or doesn't exist.");
            }

            for(let j=0; j<dataRow.length; j++){
                this[i][j] = dataRow[j];
            }
        }
    }

    public mul(num: number): Matrix{
        for(let i=0; i<this.getRows(); i++){
            for(let j=0; j<this.getCols(); j++){
                this[i][j] *= num;
            }
        }

        return this;
    }

    public getRows():number{
        return this.length;
    }

    public getCols():number{
        return this[0].length;
    }



}