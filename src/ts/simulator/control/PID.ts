

export class PID {

    private Kp: number;
    private Ki: number;
    private Kd: number;

    private error: number = 0;
    private mem_error: number = 0;
    private integral_sum: number = 0;


    constructor(kp, ki, kd: number){
        this.Kp = kp;
        this.Ki = ki;
        this.Kd = kd;
    }


    public process(control, sensor, dt: number){
        let ddt: number = dt / 1000.0;

        this.error = control - sensor;

        if(this.isOverflowAddition(this.integral_sum, this.error*ddt)){
            this.integral_sum = 0;
        }

        this.integral_sum = this.integral_sum + this.error * ddt;

        let derivative: number = (this.error - this.mem_error) / ddt;

        this.mem_error = this.error;

        return this.Kp * this.error + this.Ki * this.integral_sum + this.Kd * derivative;
    }


    private isOverflowAddition(a, b: number){
        return (a < 0.0) == (b < 0.0) && (Math.abs(b) > (Number.MAX_VALUE - Math.abs(a)));
    }
}