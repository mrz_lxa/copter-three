import {ControlMode} from "./ControlMode";

const PI = Math.PI;



let SimulationParams = {
    CONTROL_YAW_RATE: PI,   //  rad/s
    CONTROL_PR_RATE: PI,    //  rad/s
    CONTROL_PR_ANGLE: PI/4, //  rad


    WORLD_WIDTH: 50, WORLD_DEPTH: 50, WORLD_HEIGHT : 100,

    CONTROL_LINEAR_VELOCITY: 4,

    CONTROL_BASE_POWER : 0.3,

    ALTITUDE_POWER_CONTROL : true,

    CONTROL_MODE: ControlMode.ANGLE,

    ENABLE_LINEAR_MOTION: true
};



let ChartParams = {
    CHART_WIDTH: 480,
    CHART_HEIGHT: 290,

    CHART_X: 10,
    CHART_Y_BOT_OFFSET: 10,
};


export {SimulationParams, ChartParams};