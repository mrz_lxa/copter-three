import {Entity} from "./Entity";

import {SimulationParams} from "../SimulationParams";
import {ControlMode} from "../ControlMode";
import {PID} from "../control/PID";
import {CopterArm} from "./CopterArm";
import {Matrix} from "../utils/Matrix";
import {Vec2d} from "../utils/Vec2d";
import {Vec3d} from "../utils/Vec3d";
import {VMath} from "../utils/VMath";
import {Environment} from "../Environment";
import {Matrix4, Object3D} from "three";
import {Vector3} from "three";


const DEBUG_VECTORS = false;


//in METERS
const COPTER_RADIUS = 0.5; // copter radius == arm length
//in METERS
const PROPELLER_RADIUS = 0.1;

const PROPELLER_CT = 0.5; // Propeller Thrust coefficient
const PROPELLER_CQ = 4.8; // Propeller torque coefficient.

//in KILOGRAMS
const COPTER_ARM_MASS = 0.5;


//in METERS
const DEBUG_VEC_CIRCLE_RADIUS = 1.2;

const ARM_AIR_DRAG_COEF = 111.0;
const TRACTION_VALUE = 1.2014;
const MAX_VELOCITY_SCALAR = 20; // m/s

const PI = Math.PI;


const MAX_PITCH_ROLL = PI / 3;
const MAX_PITCH_ROLL_RATE = PI; //90 deg/sec

const COPTER_ARMS_COUNT = 4;


const BASE_POWER = 0.0;


const PROPELLER_RPS_MAX = 10;

class Copter extends Entity {


    private wRotation: Matrix;
    private twRotation: Matrix4;

    private dst: Vec2d;

    //world positions, velocities and accelerations
    //ANGULAR
    private copterAngularPos: Vec3d;
    private copterAngularVelocity: Vec3d;
    private copterAngularAcceleration: Vec3d;
    //LINEAR
    private linearPos: Vec3d;
    private linearVelocity: Vec3d;
    private linearAcceleration: Vec3d;


    //control yaw pitch roll velocity and pos
    private controlAngularPos: Vec3d;
    private controlAngularVelocity: Vec3d;
    private controlLinearVelocity: Vec3d;

    //difference from a cotrol destination point
    private difVec: Vec2d;
    private difNormVec: Vec2d;

    private arms: Array<CopterArm>;


    //PIDs for rate control - rates for yaw, pitch and roll channels is measured in rad/sec.

    //PIDs for rate control for 3 channels
    //when base power for all Arms is set to 0(zero === 0% power).
    private ratePidsLowPower: Array<PID>;
    //PIDs for rate control for 3 channels
    //when base power for all Arms is set to 1(one === 100% power).
    private ratePidsHighPower: Array<PID>;


    //PIDs for value control - values for yaw, pitch, roll channels. Is measured in radians.
    private valuePidsLowPower: Array<PID>;
    private valuePidsHighPower: Array<PID>;


    private pidVelocityX: PID;
    private pidVelocityY: PID;
    private pidVelocityZ: PID;



    private mesh: Object3D;
    private meshPropellers: Array<Object3D>;


    constructor(startX: number, startY: number) {
        super(startX, startY);

        //initialize position vectors
        this.linearPos = new Vec3d(startX, startY, 0);
        this.linearVelocity = new Vec3d(0, 0, 0);
        this.linearAcceleration = new Vec3d(0, 0, 0);

        this.copterAngularPos = new Vec3d(0, 0, 0);
        this.copterAngularVelocity = new Vec3d(0, 0, 0);
        this.copterAngularAcceleration = new Vec3d(0, 0, 0);

        this.controlAngularPos = new Vec3d(0, 0, 0);
        this.controlAngularVelocity = new Vec3d(0, 0, 0);
        this.controlLinearVelocity = new Vec3d(0, 0, 0);

        this.dst = new Vec2d(startX, startY);


        this.wRotation = Matrix.createMat3d();
        this.twRotation = new Matrix4();

        this.difVec = new Vec2d(0, 0);
        this.difNormVec = new Vec2d(0, 0);


        this.meshPropellers = [];


        this.initArms();
        this.initPIDs();


        console.log("COPTER CREATED");
    }



    private initPIDs() {
        this.ratePidsLowPower = [];
        this.ratePidsHighPower = [];

        this.valuePidsLowPower = [];
        this.valuePidsHighPower = [];


        this.ratePidsLowPower[0] = new PID(0.23, 0.0, 0.0001);
        this.ratePidsLowPower[1] = new PID(-0.32, 0.0, 0.0);
        this.ratePidsLowPower[2] = new PID(0.32, 0.0, 0.0);

        this.ratePidsHighPower[0] = new PID(0.007, 0, 0.0);
        this.ratePidsHighPower[1] = new PID(-0.0008, 0.0, 0.0);
        this.ratePidsHighPower[2] = new PID(0.0008, 0.0, 0.0);




        this.valuePidsLowPower[0] = null; //no yaw angle control
        this.valuePidsLowPower[1] = new PID(-3.8, 0.0, 0.0);
        this.valuePidsLowPower[2] = new PID(3.8, 0.0, 0.0);


        this.valuePidsHighPower[0] = null; //no yaw angle control
        this.valuePidsHighPower[1] = new PID(-3.8, 0.0, 0.0);
        this.valuePidsHighPower[2] = new PID(3.8, 0.0, 0.0);




        this.pidVelocityX = new PID(0.3, 0.3, 0.08);
        this.pidVelocityY = new PID(0.3, 0.3, 0.08);

        //vertical motion
        this.pidVelocityZ = new PID(0.2,0.2,0.0002);
    }


    public getCharterValues(): Array<number>{
        return [ this.getSensorAngularPos().y, this.controlAngularPos.y ];
    }



    public setMesh(mesh: Object3D){
        let me = this;

        me.mesh = mesh;

        me.meshPropellers = [];

        me.mesh.traverse(function (child: any) {
            if(child.isMesh && child.material.name === "AppDA1"){
                me.meshPropellers.push(child);
            }
        });

        me.meshPropellers.sort(function (a, b) {
            return a.id - b.id;
        });


        console.log("PROPELLERS DETECTED: ", me.meshPropellers);

    }



    private initArms() {
        this.arms = [];

        let angleBetweenArms = PI * 2 / COPTER_ARMS_COUNT;
        let angleShift = angleBetweenArms / 2;

        for (let i: number = 0; i < COPTER_ARMS_COUNT; i++) {
            this.arms.push(
                new CopterArm(
                    angleShift + angleBetweenArms * i,
                    COPTER_RADIUS, // arm length == copter radius
                    COPTER_ARM_MASS,
                    PROPELLER_RADIUS,
                    PROPELLER_CT,
                    PROPELLER_CQ,
                    i % 2 == 0
                )
            );
        }
    }


    public setVelocityControlInput(yawRate:number, xVelocity:number, yVelocity: number, thrustInput: number) {
        //yaw is forced to be controled by rate
        this.controlAngularVelocity.z = yawRate;

        this.controlLinearVelocity.x = xVelocity;
        this.controlLinearVelocity.y = yVelocity;


        this.controlLinearVelocity.z = thrustInput;
    }



    public setAngleControlInput(yawRate:number, pitchAngle:number, rollAngle: number, thrustInput: number) {
        //yaw is forced to be controlled by rate
        this.controlAngularVelocity.z = yawRate;

        this.controlAngularPos.y = pitchAngle;
        this.controlAngularPos.x = rollAngle;

        this.controlLinearVelocity.z = thrustInput;
    }


    public setRateControlInput(yawRate:number, pitchRate: number, rollRate: number, thrustInput: number) {
        this.controlAngularVelocity.z = yawRate;
        this.controlAngularVelocity.y = pitchRate;
        this.controlAngularVelocity.x = rollRate;


        this.controlLinearVelocity.z = thrustInput;
    }


    //set ANGLES directly
    setYaw(yaw: number) {
        this.copterAngularPos.z = yaw;
    }

    setPitch(pitch: number) {
        this.copterAngularPos.y = pitch;
    }

    setRoll(roll: number) {
        this.copterAngularPos.x = roll;
    }


    //set ANGLE RATES
    setYawRate(yawRate: number) {
        this.copterAngularVelocity.z = yawRate;
    }

    setPitchRate(pitchRate: number) {
        this.copterAngularVelocity.y = pitchRate;
    }

    setRollRate(rollRate: number) {
        this.copterAngularVelocity.x = rollRate;
    }


    get yaw() {
        return this.copterAngularPos.z;
    }

    get pitch() {
        return this.copterAngularPos.y;
    }

    get roll() {
        return this.copterAngularPos.x;
    }


    get pos(): Vec2d {
        return this.linearPos.xy;
    }


    public getSensorAngularPos(): Vec3d {

        let planeXYTransform = Matrix.createMat3d();
        planeXYTransform[2][2] = 0;


        //copter front and right vector in WORLD frame
        let worldDirFront = VMath.mulVecMat(Environment.DIR_FRONT, this.wRotation);
        let worldDirRight = VMath.mulVecMat(Environment.DIR_RIGHT, this.wRotation);


        //gravity vector in world frame
        let worldGravity: Vec3d = Environment.GRAVITY;


        // let yawAngle: number = VMath.getAngleBetween2d( worldDirFront.xy, Environment.DIR_FRONT.xy);
        let yawAngle: number = Math.atan2(worldDirFront.y, worldDirFront.x);


        let pitchAngle: number = -Math.asin(
            VMath.dotProduct3d(worldDirFront, worldGravity) / (worldDirFront.length * worldGravity.length)
        );


        let rollAngle: number = -Math.asin(
            VMath.dotProduct3d(worldDirRight, worldGravity) / (worldDirRight.length * worldGravity.length)
        );


        return new Vec3d(rollAngle, pitchAngle, yawAngle);
    }



    public getSensorLinearVelocity(): Vec3d {
        return VMath.mulVecMat(this.linearVelocity, VMath.matInverse(this.wRotation));
    }


    public update(dt: number) {
        this.updateProperties(dt);
        this.processControlValues(dt);

        this.updateMeshModel();
        this.rotatePropellers(dt);
    }



    private rotatePropellers(dt: number):void{
        let me = this,
            memPos = new Vector3();


        // //FRONT RIGHT PROPELLER
        // let frArm:CopterArm = me.arms[0];
        // let frPropMesh:Object3D = me.meshPropellers[1];
        // let shift: Vector3 = new Vector3(0.0, 0.0, -0.27);
        // shift.applyEuler(frPropMesh.rotation);
        // // frPropMesh.position.add(shift);
        //
        //
        // // frPropMesh.position.add( new Vector3(-10.5, 0.0, -10.5));
        // //
        // // frPropMesh.position.set(-0.5, 0.0, 0.0);
        // // (window as any).PROP = frPropMesh;
        //
        //
        // // frPropMesh.position.z = 0.27;
        // frPropMesh.rotation.y += 0.5 * PROPELLER_RPS_MAX * dt;





    }



    /**
     *
     * @param {number} dtms Delta time in milliseconds
     */
    private updateProperties(dt: number) {

        this.linearAcceleration.set(0, 0, 0);
        this.copterAngularAcceleration.set(0, 0, 0);

        let copterResultForce = new Vec3d(0, 0, 0);
        let copterResultTorque = new Vec3d(0, 0, 0);

        let copterInertiaMoment = 0;
        let copterMass = 0;

        //update arms objects
        for (let i: number = 0; i < COPTER_ARMS_COUNT; i++) {
            let arm = this.arms[i];

            arm.update();


            let worldFrameArmVec: Vec3d = VMath.mulVecMat(arm.getArmVec(), this.wRotation);


            //copter frame vectors
            let copterFrameThrustVec: Vec3d = arm.getProducedThrust();
            let copterFrameTorqueVec: Vec3d = arm.getProducedTorque();


            //world frame vectors
            let worldFrameThrustVec: Vec3d = VMath.mulVecMat(copterFrameThrustVec, this.wRotation);
            let worldFrameTorqueVec: Vec3d = VMath.mulVecMat(copterFrameTorqueVec, this.wRotation);

            let worldFrameGravityVec: Vec3d = VMath.scaleVec(Environment.GRAVITY, arm.getMass());
            let worldFrameGravityTorqueVec: Vec3d = VMath.crossProduct3d(worldFrameArmVec, worldFrameGravityVec);


            //copter frame "drag torque" vector - opposite direction to copter frame angular velocity vec
            let normCopterFrameAngularVelocityVec: Vec3d = VMath.normalize(this.copterAngularVelocity, true);
            let normCopterFrameDragVec: Vec3d = VMath.scaleVec(normCopterFrameAngularVelocityVec, -1);

            //multiply drag direction by drag
            let angularVelocityScalar: number = this.copterAngularVelocity.length;

            let dragForceScalarValue: number = ( 1 / 2 * angularVelocityScalar * angularVelocityScalar * Environment.AIR_DENCITY * ARM_AIR_DRAG_COEF);
            let copterFrameDragTorqueVec: Vec3d = VMath.scaleVec(normCopterFrameDragVec, dragForceScalarValue);
            let worldFrameDragTorqueVec: Vec3d = VMath.mulVecMat(copterFrameDragTorqueVec, this.wRotation);


            let resultForce = VMath.vec3dSum(worldFrameThrustVec, worldFrameGravityVec);
            let resultTorque = VMath.vec3dSum(worldFrameTorqueVec, worldFrameGravityTorqueVec);

            //sum up all arms force vectors to calc angular and linear accelerations
            copterResultForce.add(resultForce);
            copterResultTorque.add(resultTorque);


            copterInertiaMoment += arm.getInertiaMoment();
            copterMass += arm.getMass();
        }

        this.linearAcceleration.set(
            VMath.scaleVec(copterResultForce, 1 / copterMass)
        );
        // this.linearAcceleration.z = 0.0;


        this.copterAngularAcceleration.set(
            //translate angular acceleration back from WORLD frame to COPTER frame value
            VMath.mulVecMat(
                VMath.scaleVec(copterResultTorque, 1 / copterInertiaMoment),
                VMath.matInverse(this.wRotation)
            )
        );


        //update angle rate values (angular velocities)
        this.copterAngularVelocity.add(VMath.scaleVec(this.copterAngularAcceleration, dt));

        //update angle values
        this.copterAngularPos.add(VMath.scaleVec(this.copterAngularVelocity, dt));


        //rotate by YAW, PITCH, ROLL consequently
        let orientationChangeMatrix = VMath.mulMat(
            VMath.mulMat(
                VMath.getRotZMat3d(this.copterAngularVelocity.z * dt),
                VMath.getRotYMat3d(this.copterAngularVelocity.y * dt)
            ),
            VMath.getRotXMat3d(this.copterAngularVelocity.x * dt)
        );

        let tOrientChange = new Matrix4();

        tOrientChange.multiply(new Matrix4().makeRotationZ(-this.copterAngularVelocity.y * dt));
        tOrientChange.multiply(new Matrix4().makeRotationY(-this.copterAngularVelocity.z * dt));
        tOrientChange.multiply(new Matrix4().makeRotationX(-this.copterAngularVelocity.x * dt));


        //apply rotation to a rotation state matrix
        this.wRotation = VMath.mulMat(this.wRotation, orientationChangeMatrix);
        this.twRotation.multiply(tOrientChange);


        //make YAW, PITCH, ROLL angle to be in [0:2*PI] range
        this.copterAngularPos.z = VMath.circularBound(this.copterAngularPos.z, 0, 2 * PI);


        //constrain PITCH and ROLL angles to MAX_PITCH_ROLL value deg
        // this.copterAngularVelocity.y = VMath.constrainValue(this.copterAngularVelocity.y, -MAX_PITCH_ROLL_RATE, MAX_PITCH_ROLL_RATE);
        // this.copterAngularVelocity.x = VMath.constrainValue(this.copterAngularVelocity.x, -MAX_PITCH_ROLL_RATE, MAX_PITCH_ROLL_RATE);

        // let worldAngularPos = VMath.mulVecMat(this.controlAngularPos, this.wRotation);
        //
        // worldAngularPos.y = VMath.constrainValue(worldAngularPos.y, -MAX_PITCH_ROLL, MAX_PITCH_ROLL);
        // worldAngularPos.x = VMath.constrainValue(worldAngularPos.x, -MAX_PITCH_ROLL, MAX_PITCH_ROLL);


        // this.copterAngularPos.set(VMath.mulVecMat(worldAngularPos, VMath.matInverse(this.wRotation)));

        // if (Math.abs(this.copterAngularPos.y) == MAX_PITCH_ROLL) {
        //     this.copterAngularVelocity.y = 0;
        // }
        //
        // if (Math.abs(this.copterAngularPos.x) == MAX_PITCH_ROLL) {
        //     this.copterAngularVelocity.x = 0;
        // }

        //update velocity
        this.linearVelocity.add(VMath.scaleVec(this.linearAcceleration, dt));
        if (this.linearVelocity.length >= MAX_VELOCITY_SCALAR) {
            this.linearVelocity.set(
                VMath.scaleVec(VMath.normalize(this.linearVelocity, true), MAX_VELOCITY_SCALAR)
            );
        }



        //apply traction - decrease velocity.
        //can't decrease less than 0.
        this.applyTraction(dt);

        //update position
        if (SimulationParams.ENABLE_LINEAR_MOTION) {
            this.linearPos.add(VMath.scaleVec(this.linearVelocity, dt));
        }

        //prevent from running out of world max width and height
        this.linearPos.x = VMath.circularBound(this.linearPos.x, -SimulationParams.WORLD_WIDTH, SimulationParams.WORLD_WIDTH);
        this.linearPos.y = VMath.circularBound(this.linearPos.y, -SimulationParams.WORLD_DEPTH, SimulationParams.WORLD_DEPTH);
        this.linearPos.z = VMath.constrainValue(this.linearPos.z, 0.0, SimulationParams.WORLD_HEIGHT);


        if(this.linearPos.z === 0 || this.linearPos.z === SimulationParams.WORLD_HEIGHT){
            this.linearVelocity.z = 0;
        }


    }

    private getRateControlCorrections(dtms: number, basePower: number): Array<number>{
        return [

            //yaw rate correction
            VMath.mix(
                this.ratePidsLowPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                this.ratePidsHighPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                basePower
            ),

            //pitch roll correction
            VMath.mix(
                this.ratePidsLowPower[1].process(this.controlAngularVelocity.y, this.copterAngularVelocity.y, dtms),
                this.ratePidsHighPower[1].process(this.controlAngularVelocity.y, this.copterAngularVelocity.y, dtms),
                basePower
            ),

            //roll rate correction
            VMath.mix(
                this.ratePidsLowPower[2].process(this.controlAngularVelocity.x, this.copterAngularVelocity.x, dtms),
                this.ratePidsHighPower[2].process(this.controlAngularVelocity.x, this.copterAngularVelocity.x, dtms),
                basePower
            )
        ];
    }


    private getAngleControlCorrections(dtms: number, basePower: number): Array<number>{
        let sensorAngularPos: Vec3d = this.getSensorAngularPos();

        //calculate desired rate correction
        let pitchRateCorrection =  VMath.mix(
            this.valuePidsLowPower[1].process(this.controlAngularPos.y, sensorAngularPos.y, dtms),
            this.valuePidsHighPower[1].process(this.controlAngularPos.y, sensorAngularPos.y, dtms),
            basePower
        );

        let rollRateCorrection =  VMath.mix(
            this.valuePidsLowPower[2].process(this.controlAngularPos.x, sensorAngularPos.x, dtms),
            this.valuePidsHighPower[2].process(this.controlAngularPos.x, sensorAngularPos.x, dtms),
            basePower
        );


        return [
            //apply rate correction at yaw channel
            VMath.mix(
                this.ratePidsLowPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                this.ratePidsHighPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                basePower
            ),

            //apply angle correction on pitch and roll channels
            VMath.mix(
                this.ratePidsLowPower[1].process(pitchRateCorrection, this.copterAngularVelocity.y, dtms),
                this.ratePidsHighPower[1].process(pitchRateCorrection, this.copterAngularVelocity.y, dtms),
                basePower
            ),
            VMath.mix(
                this.ratePidsLowPower[2].process(rollRateCorrection, this.copterAngularVelocity.x, dtms),
                this.ratePidsHighPower[2].process(rollRateCorrection, this.copterAngularVelocity.x, dtms),
                basePower
            )
        ];
    }


    private getLinearVelocityControlCorrections(dtms: number, basePower:number) : Array<number>{
        let sensorAngularPos: Vec3d = this.getSensorAngularPos();
        let sensorLinearVelocity: Vec3d = this.getSensorLinearVelocity();


        //calc desired angle
        let desiredPitchAngle = this.pidVelocityX.process(sensorLinearVelocity.x, this.controlLinearVelocity.x, dtms);
        let desiredRollAngle = this.pidVelocityY.process(sensorLinearVelocity.y, this.controlLinearVelocity.y, dtms);

        //calculate desired rate correction

        let desiredPitchRate = VMath.mix(
            this.ratePidsLowPower[1].process(desiredPitchAngle, sensorAngularPos.y, dtms),
            this.ratePidsHighPower[1].process(desiredPitchAngle, sensorAngularPos.y, dtms),
            basePower
        );
        let desiredRollRate = VMath.mix(
            this.ratePidsLowPower[2].process(desiredRollAngle, sensorAngularPos.x, dtms),
            this.ratePidsHighPower[2].process(desiredRollAngle, sensorAngularPos.x, dtms),
            basePower
        );


        return [
            //apply rate correction at yaw channel
            VMath.mix(
                this.ratePidsLowPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                this.ratePidsHighPower[0].process(this.controlAngularVelocity.z, this.copterAngularVelocity.z, dtms),
                basePower
            ),


            //apply angle correction on pitch and roll channels
            VMath.mix(
                this.valuePidsLowPower[1].process(desiredPitchRate, this.copterAngularVelocity.y, dtms),
                this.valuePidsHighPower[1].process(desiredPitchRate, this.copterAngularVelocity.y, dtms),
                basePower
            ),
            VMath.mix(
                this.ratePidsLowPower[2].process(desiredRollRate, this.copterAngularVelocity.x, dtms),
                this.ratePidsHighPower[2].process(desiredRollRate, this.copterAngularVelocity.x, dtms),
                basePower
            )
        ];
    }

    private getChannelsCorrections(dtms: number, basePower: number): Array<number>{

        switch (SimulationParams.CONTROL_MODE){
            case ControlMode.RATE :{
                return this.getRateControlCorrections(dtms, basePower);
            }

            case ControlMode.ANGLE :{
                return this.getAngleControlCorrections(dtms, basePower);
            }

            case ControlMode.LINEAR_VELOCITY :{
                return this.getLinearVelocityControlCorrections(dtms, basePower);
            }

            default: return [0,0,0];
        }
    }

    private processControlValues(dts: number) {

        let dtms = dts * 1000;

        let basePower: number = BASE_POWER;

        if(SimulationParams.ALTITUDE_POWER_CONTROL){
            basePower += this.controlLinearVelocity.z;
        }else{
            basePower += this.pidVelocityZ.process(this.controlLinearVelocity.z, this.linearVelocity.z, dtms);
        }

        let channelsCorrections = this.getChannelsCorrections(dtms, basePower);

        for (let i = 0; i < COPTER_ARMS_COUNT; i++) {
            let arm = this.arms[i];

            arm.setPower(basePower);

            arm.addPowerYawCorrection(channelsCorrections[0]);
            arm.addPowerPitchCorrection(channelsCorrections[1]);
            arm.addPowerRollCorrection(channelsCorrections[2]);
        }
    }


    private updateMeshModel(): void{

        let sensAngularPos = this.getSensorAngularPos(),
            rm = this.wRotation;


        // this.twRotation.set(
        //     rm[0][0], rm[0][1], rm[0][2], 0,
        //     rm[1][0], rm[1][1], rm[1][2], 0,
        //     rm[2][0], rm[2][1], rm[2][2], 0,
        //     0, 0, 0, 1
        // );


        // this.twRotation.makeRotationFromEuler(new Euler(
        //     -sensAngularPos.x,
        //     sensAngularPos.z,
        //     sensAngularPos.y ,
        //     "ZYX"
        // ));

        this.mesh.setRotationFromMatrix(this.twRotation);
        this.mesh.position.set(this.linearPos.x, this.linearPos.z, this.linearPos.y);

        // let rZ = this.mesh.rotation.z;
        // let rY = this.mesh.rotation.z;
        //
        // this.mesh.rotation.y = rZ;
        // this.mesh.rotation.z = rY;
        // this.mesh.rotation.reorder("ZYX");
    }


    applyTraction(dt: number) {
        let velocityValue: number = this.linearVelocity.length;
        let velocityDir: Vec3d = VMath.normalize(this.linearVelocity);
        if ((velocityValue = velocityValue - TRACTION_VALUE * dt) <= 0) {
            this.linearVelocity.set(0, 0, 0);
            return;
        }
        this.linearVelocity.set(VMath.scaleVec(velocityDir, velocityValue));
    }


    setDestination(x: number, y: number) {
        this.dst.set(x, y);
    }


    public setArmPower(armIndex, power: number) {
        this.arms[armIndex].setPower(power);
    }


    public render(ctx: CanvasRenderingContext2D, infoCtx: CanvasRenderingContext2D) {
        ctx.save();

        ctx.lineWidth = 1 / Environment.PIXELS_IN_METER;

        //circle
        ctx.beginPath();
        ctx.strokeStyle = "#AAAAAA";
        ctx.arc(this.linearPos.x, this.linearPos.y, COPTER_RADIUS, 0, PI * 2);
        ctx.stroke();


        //dst point
        ctx.beginPath();
        ctx.strokeStyle = "red";
        ctx.arc(this.dst.x, this.dst.y, 1 / Environment.PIXELS_IN_METER, 0, PI * 2);
        ctx.stroke();

        for (let i: number = 0; i < COPTER_ARMS_COUNT; i++) {
            this.arms[i].render(ctx, this.linearPos, this.wRotation);
        }

        this.drawParametersValues(infoCtx);

        if (DEBUG_VECTORS) {
            this.drawVectorsCircle(ctx);
        }

        ctx.restore();
    }

    public drawParametersValues(ctx: CanvasRenderingContext2D) {

        let sensorAngularPos: Vec3d = this.getSensorAngularPos();
        let sensorLinearVelocity: Vec3d = this.getSensorLinearVelocity();




        Copter.printVectorValues(ctx, "angular pos", this.copterAngularPos, 30, 20);
        Copter.printVectorValues(ctx, "sensor angular pos", sensorAngularPos, 30, 35);
        Copter.printVectorValues(ctx, "control angular pos", this.controlAngularPos, 30, 50);

        Copter.printVectorValues(ctx, "angular velocity", this.copterAngularVelocity, 30, 80);
        Copter.printVectorValues(ctx, "control angular velocity", this.controlAngularVelocity, 30, 95);

        Copter.printVectorValues(ctx, "sensor linear velocity", sensorLinearVelocity, 30, 120);
        Copter.printVectorValues(ctx, "control linear velocity", this.controlLinearVelocity, 30, 135);



        Copter.drawText(ctx, "copter altitude", this.linearPos.z, 30, 165, 10);
        Copter.drawText(ctx, "copter vertical velocity", this.linearVelocity.z, 30, 180, 10);
    }


    private static printVectorValues(ctx: CanvasRenderingContext2D, caption: string, vec:Vec3d, x:number, y:number): void{
        Copter.drawText(ctx, caption, vec.toString(), x, y, 10);
    }


    private drawVectorsCircle(ctx: CanvasRenderingContext2D) {
        ctx.save();

        //circle
        ctx.beginPath();
        ctx.strokeStyle = "#DDDDDD";
        ctx.arc(this.linearPos.x, this.linearPos.y, DEBUG_VEC_CIRCLE_RADIUS, 0, PI * 2);

        ctx.stroke();

        ctx.restore();

        this.drawVec2d(ctx, VMath.mulVecMat(Environment.DIR_FRONT, this.wRotation).xy, "#a0a0a0", 1, 20);
        this.drawVec2d(ctx, this.linearVelocity.xy, "#1da307", 4, 300);
        this.drawVec2d(ctx, this.linearAcceleration.xy, "#3300ff", 2, 100000);
    }

    static drawText(ctx: CanvasRenderingContext2D, caption: string, value: any, x: number, y: number, fontSize: number, align?: CanvasTextAlign ) {
        ctx.save();

        ctx.beginPath();

        ctx.fillStyle = "black";
        ctx.font = fontSize + "pt Arial";
        ctx.textAlign = align ? align : "left";
        ctx.fillText(caption + ": " + value, x, y);

        ctx.restore();
    }

    private drawVec2d(ctx: CanvasRenderingContext2D, vec: Vec2d, color: string, lineWidth: number, scale: number) {
        ctx.save();


        let normVec: Vec2d = VMath.normalize(vec, true),
            start: Vec2d = VMath.vec2dSum(this.linearPos.xy, VMath.mulVec2dNum(normVec, DEBUG_VEC_CIRCLE_RADIUS)),
            end: Vec2d = VMath.vec2dSum(start, VMath.mulVec2dNum(vec, scale));

        ctx.beginPath();

        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;

        ctx.moveTo(start.x, start.y);
        ctx.lineTo(end.x, end.y);

        ctx.stroke();


        ctx.restore();
    }

}




export {Copter};