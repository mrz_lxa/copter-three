import {Entity} from "./objects/Entity";
import {PerspectiveCamera, Renderer, Scene, WebGLRenderer} from "three";
import {Copter} from "./objects/Copter";
import {Charter} from "./utils/Charter";
import {SimulationParams} from "./SimulationParams";
import {ControlMode} from "./ControlMode";
import {Environment} from "./Environment";



export class ThreeEngine {


    private entities : Array<Entity>;


    private webGLRenderer: Renderer;
    private camera : PerspectiveCamera;
    private scene: Scene;

    public screenCanvas : HTMLCanvasElement;
    private infoCanvas: HTMLCanvasElement;


    private screenCtx : CanvasRenderingContext2D;
    private infoCtx : CanvasRenderingContext2D;



    private cYawInput: number = 0;
    private cPitchInput: number = 0;
    private cRollInput: number = 0;

    private cThrustInput: number = 0;

    public copter : Copter;


    private charter: Charter;

    constructor( scene: Scene, camera : PerspectiveCamera){

        this.screenCanvas = document.createElement("canvas");
        this.infoCanvas = document.createElement("canvas");

        this.screenCtx = this.screenCanvas.getContext("2d");
        this.infoCtx = this.infoCanvas.getContext("2d");


        this.webGLRenderer = new WebGLRenderer({antialias:true});

        this.camera = camera;
        this.scene = scene;

        this.entities = [];


        this.charter = new Charter();

        this.charter.addDataStyle(0, {
            color: "red",
            lineWidth: 2
        });

        this.charter.addDataStyle(1, {
            color: "blue",
            lineWidth: 2
        });


        document.body.appendChild(this.screenCanvas);

        this.initCopter();
        this.setEventHandlers();
        this.onResize();

        console.log("ENGINE CONSTRUCTOR");
    }


    public initCopter(){
        console.log("Copter initializing...");

        this.copter = new Copter(0.0, 0.0);

        this.entities.push(this.copter);

    }



    public setEventHandlers(){
        window.addEventListener( 'mousedown', this.onMouseDown.bind(this), false);
        window.addEventListener( 'keydown', this.onKeyDown.bind(this), false);
        window.addEventListener( 'keyup', this.onKeyUp.bind(this), false);
        // window.addEventListener( 'mousemove', this.onMouseMove, false);
        // window.addEventListener( 'mouseup', this.onMouseUp, false);

        window.addEventListener("resize", this.onResize.bind(this));
        this.onResize();
    }


    public onKeyDown(e){
        this.onKeyEvent(e, true)
    }

    public onKeyUp(e){
        this.onKeyEvent(e, false)
    }


    public onKeyEvent(e, pressed: boolean){
        let keyCode = e.keyCode;

        console.log(keyCode);
        switch (keyCode){
            //forward button
            case 38:
            case 87:{
                // console.log("forward", pressed);
                this.copterForward(pressed)
            }break;

            //backward button
            case 83:
            case 40:{
                // console.log("backward", pressed);
                this.copterBackward(pressed)
            }break;

            //left button
            case 65:
            case 37:{
                // console.log("left", pressed);
                this.copterLeft(pressed)
            }break;

            //right button
            case 68:
            case 39:{
                // console.log("right", pressed);
                this.copterRight(pressed)
            }break;


            case 81:{
                // console.log("rotate CCW <<< ", pressed);
                this.copterCCW(pressed)
            }break;

            case 69:{
                // console.log("rotate CW >>>", pressed);
                this.copterCW(pressed)
            }break;


            case 32:
            case 73: {
                // console.log("go UP", pressed);
                this.copterUp(pressed);
            }break;

            case 74: {
                // console.log("go DOWN", pressed);
                this.copterDown(pressed);
            }break;



        }
    }

    public onMouseDown (e) {
        e.preventDefault();

        //left mouse button click
        if(e.button === 0){
            // console.log("lb click", e.offsetX, e.offsetY);
            this.copter.setDestination(e.offsetX / Environment.PIXELS_IN_METER, e.offsetY / Environment.PIXELS_IN_METER);
            return;
        }

        //right mouse button click
        if(e.button === 2){

            return;
        }

        return false;
    };

    public update(dt: number){

        this.updateCopterControlSignals();


        for(let i=0; i<this.entities.length; i++){
            let entity = this.entities[i];

            entity.update(dt);
        }

    }

    public render(dt: number){

        let screenWidth: number = this.screenCanvas.width;
        let screenHeight: number = this.screenCanvas.height;

        this.infoCtx.clearRect(0, 0 , screenWidth, screenHeight);
        this.screenCtx.clearRect(0, 0 , screenWidth, screenHeight);


        this.webGLRenderer.render(this.scene, this.camera);


        this.charter.addValues(Date.now(), this.copter.getCharterValues());


        this.charter.render(this.infoCtx, screenWidth, screenHeight);


        this.screenCtx.drawImage(this.webGLRenderer.domElement, 0, 0);
        this.screenCtx.drawImage(this.infoCanvas, 0, 0);
    }


    public onResize(){
        let newWidth = window.innerWidth,
            newHeight = window.innerHeight,

            aspect = newWidth / newHeight;

        this.screenCanvas.width = newWidth;
        this.screenCanvas.height = newHeight;

        this.infoCanvas.width = newWidth;
        this.infoCanvas.height = newHeight;

        this.webGLRenderer.setSize(newWidth, newHeight);


        this.camera.aspect = aspect;
        this.camera.updateProjectionMatrix();
    }



    public updateCopterControlSignals(){
        switch(SimulationParams.CONTROL_MODE){
            case ControlMode.RATE:{
                this.copter.setRateControlInput(this.cYawInput, this.cPitchInput, this.cRollInput, this.cThrustInput);
            }break;

            case ControlMode.ANGLE:{
                this.copter.setAngleControlInput(this.cYawInput, this.cPitchInput, this.cRollInput, this.cThrustInput);
            }break;

            case ControlMode.LINEAR_VELOCITY:{
                this.copter.setVelocityControlInput(this.cYawInput, this.cPitchInput, this.cRollInput, this.cThrustInput);
            }break;
        }
    }



    public copterForward(enable:boolean){
        let enableFlag: number = enable ? 1 : 0;

        let inputValue: number;

        switch (SimulationParams.CONTROL_MODE){
            case ControlMode.RATE:{
                inputValue = SimulationParams.CONTROL_PR_RATE;
            }break;

            case ControlMode.ANGLE:{
                inputValue = -SimulationParams.CONTROL_PR_ANGLE;
            }break;

            case ControlMode.LINEAR_VELOCITY:{
                inputValue = SimulationParams.CONTROL_LINEAR_VELOCITY;
            }break;
        }

        this.cPitchInput = enableFlag * inputValue;
    }

    public copterBackward(enable:boolean){
        let enableFlag: number = enable ? 1 : 0;

        let inputValue: number;

        switch (SimulationParams.CONTROL_MODE){
            case ControlMode.RATE:{
                inputValue = SimulationParams.CONTROL_PR_RATE;
            }break;

            case ControlMode.ANGLE:{
                inputValue = -SimulationParams.CONTROL_PR_ANGLE;
            }break;

            case ControlMode.LINEAR_VELOCITY:{
                inputValue = SimulationParams.CONTROL_LINEAR_VELOCITY;
            }break;
        }

        this.cPitchInput = enableFlag * -inputValue;
    }

    public copterLeft(enable:boolean){
        let enableFlag: number = enable ? 1 : 0;

        let inputValue: number;

        switch (SimulationParams.CONTROL_MODE){
            case ControlMode.RATE:{
                inputValue = SimulationParams.CONTROL_PR_RATE;
            }break;

            case ControlMode.ANGLE:{
                inputValue = SimulationParams.CONTROL_PR_ANGLE;
            }break;

            case ControlMode.LINEAR_VELOCITY:{
                inputValue = -SimulationParams.CONTROL_LINEAR_VELOCITY;
            }break;
        }

        this.cRollInput = enableFlag * inputValue;
    }

    public copterRight(enable:boolean){
        let enableFlag: number = enable ? 1 : 0;

        let inputValue: number;

        switch (SimulationParams.CONTROL_MODE){
            case ControlMode.RATE:{
                inputValue = SimulationParams.CONTROL_PR_RATE;
            }break;

            case ControlMode.ANGLE:{
                inputValue = SimulationParams.CONTROL_PR_ANGLE;
            }break;

            case ControlMode.LINEAR_VELOCITY:{
                inputValue = -SimulationParams.CONTROL_LINEAR_VELOCITY;
            }break;
        }

        this.cRollInput = enableFlag * -inputValue;
    }

    public copterCW(enable:boolean){
        this.cYawInput = enable ? SimulationParams.CONTROL_YAW_RATE : 0;
    }

    public copterCCW(enable:boolean){
        this.cYawInput = enable ? -SimulationParams.CONTROL_YAW_RATE : 0;
    }


    public copterUp(enabled: boolean){
        let valueInput = SimulationParams.ALTITUDE_POWER_CONTROL ? SimulationParams.CONTROL_BASE_POWER : SimulationParams.CONTROL_LINEAR_VELOCITY;
        this.cThrustInput = enabled ? valueInput : 0;
    }

    public copterDown(enabled: boolean){

        let valueInput = SimulationParams.ALTITUDE_POWER_CONTROL ? 0 : -SimulationParams.CONTROL_LINEAR_VELOCITY;
        this.cThrustInput = enabled ? valueInput : 0;
    }


}